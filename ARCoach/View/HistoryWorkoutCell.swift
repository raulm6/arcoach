//
//  HistoryWorkoutCell.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/29/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class HistoryWorkoutCell: UITableViewCell {

    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
