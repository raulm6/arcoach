//
//  MyBodyInputTableViewCell_age.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class MyBodyInputTableViewCell_age: UITableViewCell {

    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(text: String?, placeholder: String, value : String = "") {
        
        label.text = text
        label.accessibilityValue = text
        ageLabel.text = value
        ageLabel.accessibilityValue = value
        
        
    }

}
