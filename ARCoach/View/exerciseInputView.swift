//
//  exerciseInputView.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/28/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class exerciseInputView: UIView, UITableViewDelegate, UITableViewDataSource {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    
    public var parentWorkout : WorkoutInstance! = nil
    public var exercise : Exercise! = nil
    @IBOutlet weak var completedExerciseTable: UITableView!
    @IBOutlet weak var repTextField: UITextField!
    @IBOutlet weak var repStepper: UIStepper!
    @IBOutlet weak var weightTextfield: UITextField!
    @IBOutlet weak var weightStepper: UIStepper!
    @IBOutlet weak var exerciseName: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet private var contentView:UIView?
    // other outlets
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("exerciseInputView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        
        completedExerciseTable.delegate = self
        completedExerciseTable.dataSource = self
        
    }
    
    public func assignExerciseType(exercise : Exercise) {
        self.exercise = exercise
        self.exerciseName.text = self.exercise.name
    }
    
    public func assignParentWorkout(workout : WorkoutInstance) {
        self.parentWorkout = workout
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercise.completedExercises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "exericseInputCell")
        
        
        
        cell.textLabel?.text = "Reps: " + String(exercise.completedExercises[indexPath.row].reps) + " Weight: " + String(exercise.completedExercises[indexPath.row].weight)
        
        return cell
    }
    
    // Delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            exercise.completedExercises.remove(at: indexPath.row)
            completedExerciseTable.reloadData()
            
        }
    }
    
    @IBAction func addPressed(_ sender: UIButton) {
        print("DEBUG: Add pressed")
       
        if let repInput = repTextField.text, let weightInput = weightTextfield.text  {
            let repsValue = Int(repInput)
            let weightValue = Double(weightInput)
            if let finalReps = repsValue, let finalWeight = weightValue {                
                self.exercise.spawnAndAddExerciseInstance(reps : finalReps, weight : finalWeight)
                self.completedExerciseTable.reloadData()
                print("DEBUG: Successful add")
            }
        }
        
        
        
        
        
    }
    
    @IBAction func repStepperChanged(_ sender: UIStepper) {
        repTextField.text = Int(sender.value).description
    }
    
    @IBAction func weightStepperChanged(_ sender: UIStepper) {
        weightTextfield.text = Int(sender.value).description
    }
    
    public func resetCompletedExerciseTable() {
        completedExerciseTable.reloadData()
    }
}
