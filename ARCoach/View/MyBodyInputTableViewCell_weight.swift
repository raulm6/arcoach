//
//  MyBodyInputTableViewCell_weight.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class MyBodyInputTableViewCell_weight: UITableViewCell {
    @IBOutlet weak var label: UILabel!

    
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(text: String?, placeholder: String, value : String = "") {        
        label.text = text
        label.accessibilityValue = text
        valueLabel.text = value
        valueLabel.accessibilityValue = value
    }

}
