//
//  WorkoutChosenViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/12/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class WorkoutChosenViewController: UIViewController {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var stackView: UIStackView!
    var workoutObject : WorkoutInstance! = nil
    @IBOutlet weak var workoutName: UILabel!
    @IBAction func save(_ sender: UIButton) {
        
        let successfulSave = saveWorkoutInstance()
        
        var alertMessage : String = ""
        if successfulSave {
            alertMessage = "Successful Save!"
            print("DEBUG: Successful Save")
        } else {
            alertMessage = "Error: There was an Error Saving your data :("
        }
                    
        let alertController = UIAlertController(title: "Save Result", message:
            alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func saveAndFinish(_ sender: UIButton) {
        
        let successfulSave = saveWorkoutInstance()
        
        var alertMessage : String = ""
        if successfulSave {
            alertMessage = "Successful Save!"
            print("DEBUG: Successful Save")
        } else {
            alertMessage = "Error: There was an Error Saving your data :("
        }
        
        print("DEBUG: Successful Save")
        let alertController = UIAlertController(title: "Your workout will be finalized, are you sure?", message:
            alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {_ in
            self.navigateBack()
            return
        }))
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
        //self.dismiss(animated: true, completion: nil)
        
    }
    
    public func navigateBack() -> Void {
        CATransaction.setCompletionBlock({
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    
    @IBAction func reset(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Reset Workout Instance", message: "Current progress will be lost", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {_ in
            self.resetWorkoutInstance()
            return
        }))
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        
        
        self.present(alertController, animated: true, completion: nil)
        
        

        
        
    }
    
    
    private func saveWorkoutInstance() -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let successfulSave = appDelegate.coreDataController?.saveWorkoutInstance(workout: self.workoutObject)
        return successfulSave!
    }
    
    public func resetWorkoutInstance() {
        for exercise in self.workoutObject.exerciseArray {
            exercise.completedExercises = []
        }
        
        var i = 0
        for childView in self.stackView.arrangedSubviews {
            let castedChildView = childView as? exerciseInputView
            print("view : " + String(i))
            i += 1
            if let unwrapped = castedChildView {
                unwrapped.resetCompletedExerciseTable()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Add A Bunch of subViews with Exercises
        
        for exercise in workoutObject.exerciseArray {
            let newView = exerciseInputView()
            newView.heightAnchor.constraint(equalToConstant: 510).isActive = true
            newView.assignExerciseType(exercise: exercise)
            newView.assignParentWorkout(workout: workoutObject)
            self.stackView.addArrangedSubview(newView)
        }
        
        self.workoutName.text = self.workoutObject.name
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("DEBUG: Chosen Workout Screen will DISAPPEAR")
        
        print("Instance GUID: \(self.workoutObject.guid)")
        
        /*
        for exercise in self.workoutObject.exerciseArray {
            for completedExercise in exercise.completedExercises {
                print("reps: \(completedExercise.reps)")
            }
        }
         */
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
