//
//  LoginViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/22/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import CoreData


class LoginViewController: UIViewController,  UITextFieldDelegate {
    

    @IBOutlet weak var cancelButton: UIButton!
    
    //@IBOutlet weak var fbLoginContainer: UIView!
    @IBOutlet weak var placeHolder: UILabel!
    
    var fbLoginButton : FBSDKLoginButton! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()        

        //self.fbLoginButton = FBSDKLoginButton()
        //self.fbLoginButton.readPermissions = ["public_profile", "email", "user_friends"]
        //self.fbLoginButton.center = self.placeHolder.center
        //self.view.addSubview(fbLoginButton)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        /*
        if let token = FBSDKAccessToken.current(), let appID = token.appID {
                //debugLabel.text = appID
        } else {
            //debugLabel.text = "Not Logged In"
        }
        */
    }
    @IBAction func createNewAccountPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "createNewUserSegue", sender: sender)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
