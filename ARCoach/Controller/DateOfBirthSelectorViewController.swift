//
//  DateOfBirthSelectorViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class DateOfBirthSelectorViewController: UIViewController {
    
    var tableObject : MyBodyTableObject?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func confirmPressed(_ sender: UIButton) {
        print("DEBUG: Confirm Pressed")
        
        let date = datePicker.date
                                
        // Pass Value to Table Source
        self.tableObject?.fieldDictionary["dateOfBirth"] =
            self.tableObject?.date = date
        
        // Save to CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.coreDataController?.saveTableObjectToUser(tableObject: self.tableObject!, guid: (appDelegate.ARCoachUserSession?.guid)!)
        
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
