//
//  WorkoutsViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/12/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class WorkoutsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var workoutsTableView: UITableView!
    var workoutsArray = Array(Array(WorkoutData().workoutDictionary).reversed())
    var selectedWorkout : Workout!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        selectedWorkout = nil
        workoutsTableView.delegate = self;
        workoutsTableView.dataSource = self;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        workoutsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = workoutsTableView.dequeueReusableCell(withIdentifier: "WorkoutCellBig", for: indexPath) as! WorkoutCellTableViewCell
        
        // Configure the cell...
        //cell.pictureView
        cell.titleLabel.text = workoutsArray[indexPath.row].value.name
        
        cell.subtitleLabel.text = ""//workoutsArray[indexPath.row].value.guid
        

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "WorkoutChosen") as! WorkoutChosenViewController
        navigationController?.pushViewController(destination, animated: true)
        
        */
        
        selectedWorkout = workoutsArray[indexPath.row].value
        performSegue(withIdentifier: "choseWorkout", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "choseWorkout" {
            // Setup new view controller
            
            let destination = segue.destination as! WorkoutChosenViewController
            let spawned = self.selectedWorkout.spawnWorkoutInstance()
            
            destination.workoutObject = spawned
            print("before egue \(self.selectedWorkout.name)")
            print("Preparing for segue transition")
        }
    }
    

}
