//
//  MyBodyViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/23/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class MyBodyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var inputTable: UITableView!
    
    let tableObject = MyBodyTableObject()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableObject.fieldOrderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableObject.fieldOrderArray[indexPath.row] {
        case "height":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBodyInputCell_height") as! MyBodyInputTableViewCell_height
            
            if let heightValue = self.tableObject.fieldDictionary["height"] as! String? {
                cell.configure(text: "Height", placeholder: "", value: heightValue)
            } else {
                cell.configure(text: "Height", placeholder: "", value: "Tap to enter")
            }
                                    
            return cell
        case "weight":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBodyInputCell_weight") as! MyBodyInputTableViewCell_weight            
            if let weightValue = self.tableObject.fieldDictionary["weight"] as! String? {
                cell.configure(text: "Weight", placeholder: "", value: weightValue)
            } else {
                cell.configure(text: "Weight", placeholder: "", value: "Tap to enter")
                
            }
            
            return cell
        case "dateOfBirth":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBodyInputCell_age") as! MyBodyInputTableViewCell_age
            
            if let dateValue = self.tableObject.date {
                
                // Get current age
                let calendar = NSCalendar.current
                
                let date1 = dateValue
                let date2 = calendar.startOfDay(for: Date())
                
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                
                if let days = components.day {
                    let displayedAge = String(format: "%.2f", Double(days) / 365.0)
                    cell.configure(text: "Age", placeholder: "", value: displayedAge)
                } else {
                    cell.configure(text: "Age", placeholder: "", value: "Tap to enter")
                }
                
            } else {
                cell.configure(text: "Age", placeholder: "", value: "Tap to enter")
            }
            
            return cell
        case "gender":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBodyInputCell_gender") as! MyBodyInputTableViewCell_gender
            
            if let genderValue = self.tableObject.fieldDictionary["gender"] as! String? {
                cell.configure(text: "Gender", placeholder: "", value : genderValue)
            } else {
                cell.configure(text: "Gender", placeholder: "", value : "Tap to enter")
            }
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBodyInputCell_gender") as! MyBodyInputTableViewCell_gender
            cell.configure(text: "Default Cell", placeholder: "default")
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "heightSelector", sender: self)
        case 1:
            performSegue(withIdentifier: "weightSelector", sender: self)
        case 2:
            performSegue(withIdentifier: "dateOfBirthSelector", sender: self)
        case 3:
            performSegue(withIdentifier: "genderSelector", sender: self)
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueId = segue.identifier {
            switch segueId {
            case "weightSelector":
                let destination = segue.destination as! WeightPickerViewController
                destination.tableObject = self.tableObject
            case "heightSelector":
                let destination = segue.destination as! HeightPickerViewController
                destination.tableObject = self.tableObject
            case "dateOfBirthSelector":
                let destination = segue.destination as! DateOfBirthSelectorViewController
                destination.tableObject = self.tableObject
                
                /*
                if let datePicker = destination.datePicker, let date = self.tableObject.date {
                    destination.datePicker.setDate(date, animated: true)
                }
                 */
                
            case "genderSelector":
                let destination = segue.destination as! GenderSelectorViewController
                destination.tableObject = self.tableObject
            default:
                print("DEBUG: Default Segue")
            }
        }        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let userUUID = appDelegate.ARCoachUserSession?.guid {
            self.tableObject.populateWithUserData(guid: userUUID)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        inputTable.reloadData()        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
