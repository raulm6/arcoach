//
//  HeightPickerViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class HeightPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var feetPicker: UIPickerView!
    @IBOutlet weak var inchesPicker: UIPickerView!
    var tableObject : MyBodyTableObject?
    
    @IBAction func confirmPressed(_ sender: UIButton) {
        print("DEBUG: Confirm Pressed")
        
        let selectedValueFeet = feetPicker.selectedRow(inComponent: 0)
        let selectedValueInches = inchesPicker.selectedRow(inComponent: 0)
        
        let selectedDouble = Double(selectedValueFeet * 12 + selectedValueInches)
        
        // Pass Value to Table Source
        self.tableObject?.fieldDictionary["height"] = String(Int(selectedDouble)) as String?
        
        // Save to CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.coreDataController?.saveTableObjectToUser(tableObject: self.tableObject!, guid: (appDelegate.ARCoachUserSession?.guid)!)
        
        self.dismiss(animated: true)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        switch pickerView.tag {
        case 0:
            return 1
        case 1:
            return 1
        default:
            return 1
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return 9
        case 1:
            return 12
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      
        
        return String(row)
        
        
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
