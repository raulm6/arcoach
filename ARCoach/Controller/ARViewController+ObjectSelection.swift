/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Methods on the main view controller for handling virtual object loading and movement
*/

import UIKit
import SceneKit

extension ARViewController: VirtualObjectSelectionViewControllerDelegate {
    func virtualObjectSelectionViewController(_ selectionViewController: VirtualObjectSelectionViewController, didSelectObject: VirtualObject) {
        return
    }
    
    func virtualObjectSelectionViewController(_ selectionViewController: VirtualObjectSelectionViewController, didDeselectObject: VirtualObject) {
        return
    }
    
    /**
     Adds the specified virtual object to the scene, placed using
     the focus square's estimate of the world-space position
     currently corresponding to the center of the screen.
     
     - Tag: PlaceVirtualObject
     */
    func placeVirtualObject(_ virtualObject: VirtualObject) {
        print("DEBUG: Place Virtual Object")
        guard let cameraTransform = session.currentFrame?.camera.transform,
            let focusSquarePosition = focusSquare.lastPosition else {
            statusViewController.showMessage("CANNOT PLACE OBJECT\nTry moving left or right.")
            virtualObjectLoader.removeAllVirtualObjects()
            return
        }
        
        
        
        
        virtualObjectInteraction.selectedObject = virtualObject
        virtualObject.setPosition(focusSquarePosition, relativeTo: cameraTransform, smoothMovement: false)
        
        /*
        let nodeArray : [SCNNode] = virtualObject.childNodes
        for childNode in nodeArray {
            let coachRefNode = childNode as! SCNReferenceNode
            coachRefNode.setPosition(focusSquarePosition, relativeTo: cameraTransform, smoothMovement: false)
            coachRefNode.simdPosition
        }
 */
        updateQueue.async {
            self.sceneView.scene.rootNode.addChildNode(virtualObject)
        }
    }
    
    // MARK: Object Loading UI

    func displayObjectLoadingUI() {
        // Show progress indicator.
        //spinner.startAnimating()
        
        //addObjectButton.setImage(#imageLiteral(resourceName: "ring"), for: [])

        //addObjectButton.isEnabled = false
        //isRestartAvailable = false
    }

    func hideObjectLoadingUI() {
        // Hide progress indicator.
        //spinner.stopAnimating()

        //addObjectButton.setImage(#imageLiteral(resourceName: "add"), for: [])
        //addObjectButton.setImage(#imageLiteral(resourceName: "addPressed-1"), for: [.highlighted])

        //addObjectButton.isEnabled = true
        //isRestartAvailable = true
    }
}
