//
//  WeightPickerViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class WeightPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var tableObject : MyBodyTableObject?
    
    @IBOutlet weak var decimalPicker: UIPickerView!
    @IBOutlet weak var picker: UIPickerView!
    @IBAction func confirmPressed(_ sender: UIButton) {
        
        
        let selectedDecimal = decimalPicker.selectedRow(inComponent: 0)
        
        let selectedValue = picker.selectedRow(inComponent: 0)
        let selectedDouble = Double(selectedValue) + Double(selectedDecimal) / 10.0
        
        // Pass Value to Table Source
        self.tableObject?.fieldDictionary["weight"] = String(selectedDouble) as String?
        
        // Save to CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                
        appDelegate.coreDataController?.saveTableObjectToUser(tableObject: self.tableObject!, guid: (appDelegate.ARCoachUserSession?.guid)!)
        
        // Dismiss segue
        self.dismiss(animated: true)
                        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 0:
            return 500
        case 1:
            return 10
        default:
            return 500
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(row)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentWeight = self.tableObject?.fieldDictionary["weight"] as! String?, let intValue = Int(currentWeight)
        {
            self.picker.selectRow(intValue, inComponent: 0, animated: false)
        } else {
            self.picker.selectRow(150, inComponent: 0, animated: false)
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
