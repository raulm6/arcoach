//
//  HistoryWorkoutViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/29/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class HistoryWorkoutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("There are \(self.workoutInstance.exerciseArray[0].completedExercises.count) cells")
    return self.workoutInstance.exerciseArray[0].completedExercises.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loggedInstanceCell") as! UITableViewCell
        let nameValue = workoutInstance.exerciseArray[0].completedExercises[indexPath.row].name
        let repValue = String(workoutInstance.exerciseArray[0].completedExercises[indexPath.row].reps)
        let weightValue =        String(workoutInstance.exerciseArray[0].completedExercises[indexPath.row].weight)
        
        cell.textLabel?.text = "Exercise: \(nameValue)\r\nReps: \(repValue)\r\nWeight: \(weightValue)"
        
        return cell
    }
    

    var workoutGuid : String? = nil
    var workoutInstance : WorkoutInstance! = nil
    
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var exerciseInstanceTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.workoutInstance = appDelegate.coreDataController?.fetchWorkoutInstance(self.workoutGuid!)
        self.workoutNameLabel.text = self.workoutInstance.name
        self.dateLabel.text = self.workoutInstance.dateString
        print("DEBUG: Workout Date: \(String(describing: self.workoutInstance.dateString))")
        self.workoutInstance.exerciseArray[0].completedExercises.sort(by: { (e1 : ExerciseInstance, e2 : ExerciseInstance) in
            return e1.guid < e2.guid
        })
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
