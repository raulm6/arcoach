//
//  HomeViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/27/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var previousWorkoutsTable: UITableView!
    
    var selectedWorkoutGuid : String? = nil
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let userGuid = appDelegate.ARCoachUserSession?.guid as! String
        let workoutInstancesArray = appDelegate.coreDataController?.fetchWorkoutsForUser(userGuid: userGuid)
        
        return workoutInstancesArray!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryWorkoutCell") as! HistoryWorkoutCell
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let userGuid = appDelegate.ARCoachUserSession?.guid as! String
        let workoutInstancesArray = appDelegate.coreDataController?.fetchWorkoutsForUser(userGuid: userGuid) as! [WorkoutInstance]
        
        let workoutInstance = workoutInstancesArray[indexPath.row]
        
        
        //cell.titleLabel.text = "Treasure"
        
        cell.titleLabel.text = workoutInstance.name
        cell.subtitleLabel.text = workoutInstance.dateString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let userGuid = appDelegate.ARCoachUserSession?.guid as! String
        let workoutInstancesArray = appDelegate.coreDataController?.fetchWorkoutsForUser(userGuid: userGuid) as! [WorkoutInstance]
        
        let workoutInstance = workoutInstancesArray[indexPath.row]
        
        self.selectedWorkoutGuid = workoutInstance.instanceGuid as! String
        
        performSegue(withIdentifier: "historyWorkoutSegue", sender: self)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let tag = segue.identifier {
            switch tag {
                
            case "historyWorkoutSegue" :
                let destination = segue.destination as! HistoryWorkoutViewController
                destination.workoutGuid = self.selectedWorkoutGuid
                print("DEBUG: Workout instance GUID \(destination.workoutGuid)")
            
            default:
                print("no tag")
            }
        }
        
        
    }
    

    @IBOutlet weak var logButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        previousWorkoutsTable.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func logButtonPressed(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "loginSegue", sender: sender)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
