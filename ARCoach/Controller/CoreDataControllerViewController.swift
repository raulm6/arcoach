//
//  CoreDataControllerViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit
import CoreData

class CoreDataControllerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func handleAppOpening() {
        
        print("DEBUG: Core Data Controller Loaded")
        // MARK: - Get User Session
        
        // 0. get App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // 1. Get Context
        let context = appDelegate.persistentContainer.viewContext
        
        // 2. Build request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        request.returnsObjectsAsFaults = false
        
        // 3. Execute request
        do {
            let results = try context.fetch(request) as! [NSManagedObject]
            
            if results.count > 1 {
                print("DEBUG: more than 1 user stored")
                /*
                 guard false else {
                 fatalError("""
                 How do we know which one is the right user?
                 """)
                 }
                 */
            } else if results.count == 1 {
                print("DEBUG: one user found")
                let userUUID = results[0].value(forKey: "guid")
                print("The Users UUID is \(String(describing: userUUID))")
                appDelegate.ARCoachUserSession?.guid = userUUID as? String
                
            } else {
                print("DEBUG: No users registered")
                
                let newUser = NSEntityDescription.insertNewObject(forEntityName: "User", into: context)
                
                let newUUID : String = NSUUID().uuidString
                newUser.setValue(newUUID, forKey: "guid")
                
                do {
                    try context.save()                    
                    appDelegate.ARCoachUserSession?.guid = newUUID
                    
                    print("DEBUG: User Saved")
                } catch {
                    print("New User coulnd't be saved")
                }
            }
        } catch {
            print("DEBUG: Error Fetching Users for User Session")
        }
        
        
        // Load CoreData DB with Initial Workout Entities
        let initialLoadedFlag = UserDefaults.standard.object(forKey: "InitialDataLoaded")
        
        if true || initialLoadedFlag == nil || initialLoadedFlag as! Bool == false {
            print("DEBUG: Loading CoreData with Workout Entities")
            
            let exerciseData = ExerciseData()
            
            let exerciseArray = Array(exerciseData.exerciseDictionary)
            for exercise in exerciseArray {
                
                let newExerciseEntry = NSEntityDescription.insertNewObject(forEntityName: "Exercises", into: context)
                newExerciseEntry.setValue(exercise.value.name, forKey: "name")
                newExerciseEntry.setValue(exercise.value.guid, forKey: "guid")
                
            }
            
            // Save
            do {
                try context.save()
            } catch {
                print("DEBUG: Error in saving preloaded exercises")
            }
            
            let workoutData = WorkoutData()
            let workoutArray = Array(workoutData.workoutDictionary)
            for workout in workoutArray {
                
                let newWorkoutEntry = NSEntityDescription.insertNewObject(forEntityName: "Workouts", into: context)
                newWorkoutEntry.setValue(workout.value.name, forKey: "name")
                newWorkoutEntry.setValue(workout.value.guid, forKey: "guid")
                
            }
            
            // Save
            do {
                try context.save()
            } catch {
                print("DEBUG: Error in saving preloaded workouts")
            }
            
            
            UserDefaults.standard.set(true, forKey: "InitialDataLoaded")
            
        } else {
            print("DEBUG: CoreData with Workout Entities was already loaded")
        }
        
        
        
    }
    
    func getUserEntity(guid : String) -> NSManagedObject? {
        
        // 0. get App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // 1. Get Context
        let context = appDelegate.persistentContainer.viewContext
        
        // 2. Build request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        request.returnsObjectsAsFaults = false
        
        // 3. Execute request
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    
                    if (result.value(forKey: "guid") as! String == guid) {
                        
                        return result
                        
                    }
                }
                return nil
            }
            
        } catch {
            print("DEBUG: Error Fetching Users for User Session")
            return nil
        }
        
        return nil
    }
    
    func saveTableObjectToUser(tableObject : MyBodyTableObject, guid : String) {
        // 0. get App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // 1. Get Context
        let context = appDelegate.persistentContainer.viewContext
        
        // 2. Build request
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        request.returnsObjectsAsFaults = false
        
        // 3. Execute request
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if (result.value(forKey: "guid") as! String == guid) {
                        
                        if let weightValue = tableObject.fieldDictionary["weight"] as! String?, let weightDouble = Double(weightValue) {
                            //print("Weight Value: \(weightDouble)")
                                result.setValue(weightDouble, forKey: "weight")
                        }
                        
                        if let heightValue = tableObject.fieldDictionary["height"] as! String?, let heightDouble = Double(heightValue) {
                            result.setValue(heightDouble, forKey: "height")
                        }
                        
                        if let dateOfBirthValue = tableObject.date {
                            result.setValue(dateOfBirthValue, forKey: "dateOfBirth")
                        }
                        
                        if let genderValue = tableObject.fieldDictionary["gender"] as! String? {
                            result.setValue(genderValue, forKey: "gender")
                        }
                        
                        do {
                            try context.save()
                        } catch {
                            
                        }
                        
                        
                    }
                }
        
            }
        } catch {
            print("DEBUG: Error Fetching Users for User Session")
        
        }
        
    }
    
    func saveWorkoutInstance(workout : WorkoutInstance) -> Bool {
                
        // 0. Connect to context
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var userGuid : String
        if let sessionGuid = appDelegate.ARCoachUserSession?.guid {
            userGuid = sessionGuid
        } else {
            userGuid = ""
        }
        
        
        let context = appDelegate.persistentContainer.viewContext
        
        // 1. Insert Workout Instance
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WorkoutInstances")
        request.predicate = NSPredicate(format: "guid = %@", workout.instanceGuid)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                // If workout was previously saved, overwrite it with updated values
                
                let managedWorkout = results[0] as! NSManagedObject
                
                managedWorkout.setValue(Date(), forKey: "dateFinished")
                managedWorkout.setValue(workout.guid, forKey: "workoutTypeGuid")
                managedWorkout.setValue(workout.instanceGuid, forKey: "guid")
                managedWorkout.setValue(userGuid, forKey: "userGuid")
                
                do {
                    try context.save()
                } catch {
                    print("DEBUG: Error Saving New Workout")
                    return false
                }
                
            } else {
                // Create a new workout in CoreData
                let newWorkout = NSEntityDescription.insertNewObject(forEntityName: "WorkoutInstances", into: context)
                newWorkout.setValue(Date(), forKey: "dateFinished")
                newWorkout.setValue(workout.guid, forKey: "workoutTypeGuid")
                newWorkout.setValue(workout.instanceGuid, forKey: "guid")
                newWorkout.setValue(userGuid, forKey: "userGuid")
                
                do {
                    try context.save()
                } catch {
                    print("DEBUG: Error Saving New Workout")
                    return false
                }
                
            }
            
            
        } catch {
            print("Error Saving Workout")
            return false
        }
        
        // 1.5 Delete previous exercise instances associated with the workout instance
        if !deleteExerciseInstances(workoutInstanceGuid: workout.instanceGuid) {
            return false
        }
        
        
        // 2. Save Exercise Instances
        for exercise in workout.exerciseArray {
            for exerciseInstance in exercise.completedExercises {
                if !saveExerciseInstance(exerciseInstance, parentWorkoutGuid: workout.instanceGuid) {
                    return false
                }
            }
        }
        
        return true
    }
    
    public func saveExerciseInstance( _ exerciseInstance : ExerciseInstance,  parentWorkoutGuid : String = "") -> Bool {
        
        // 0. Connect to context
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var userGuid : String
        if let sessionGuid = appDelegate.ARCoachUserSession?.guid {
            userGuid = sessionGuid
        } else {
            userGuid = ""
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        // 1. Insert Exercise Instance
        let newExercise = NSEntityDescription.insertNewObject(forEntityName: "ExerciseInstances", into: context)
        newExercise.setValue(Date(), forKey: "date")
        newExercise.setValue(exerciseInstance.guid, forKey: "exerciseTypeGuid")
        newExercise.setValue(exerciseInstance.instanceGuid, forKey: "guid")
        newExercise.setValue(exerciseInstance.weight, forKey: "weight")
        newExercise.setValue(exerciseInstance.reps, forKey: "reps")
        newExercise.setValue(parentWorkoutGuid, forKey: "parentWorkoutInstance")
        
        do {
            try context.save()
        } catch {
            print("DEBUG: Failed saving exerciseInstance")
            return false
        }
        
        return true
        
    }
    
    public func deleteExerciseInstances(workoutInstanceGuid : String) -> Bool {
        // 0. Connect to context
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // 1. Delete Exercise Instance
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseInstances")
        request.predicate = NSPredicate(format: "guid = %@", workoutInstanceGuid)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            return true
        } catch {
            print("DEBUG: Couldn't delete Exercise Instances")
            return false
        }
        
    }
    
    public func fetchWorkoutsForUser(userGuid : String) -> [WorkoutInstance]{
        
        // 0. Connect to context
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var userGuid : String
        if let sessionGuid = appDelegate.ARCoachUserSession?.guid {
            userGuid = sessionGuid
        } else {
            userGuid = ""
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        // 1. Insert Workout Instance
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WorkoutInstances")
        request.predicate = NSPredicate(format: "userGuid = %@", userGuid)
        request.returnsObjectsAsFaults = false
        
        
        var output = [WorkoutInstance]()
        
        do {
            
            let results = try context.fetch(request) as! [NSManagedObject]
            for result in results {
                
                let workoutTypeGuid = result.value(forKey: "workoutTypeGuid") as! String
                let instanceGuid = result.value(forKey: "guid") as! String
                let instanceDate = result.value(forKey: "dateFinished") as! Date
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date
                formatter.dateFormat = "dd-MMM-yyyy"
                // again convert your date to string
                let instanceDateString = formatter.string(from: instanceDate)
                
                var workoutName : String = ""
                // get name
                
                do {
                    
                    let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Workouts")
                    request2.predicate = NSPredicate(format: "guid = %@", workoutTypeGuid)
                    request2.returnsObjectsAsFaults = false
                    
                    let results2 = try context.fetch(request2)
                    let result2 = results2[0] as! NSManagedObject
                    
                    workoutName = result2.value(forKey: "name") as! String
                    
                    
                    
                    
                } catch{
                    
                
                }
 
                
                
                let storedWorkout = WorkoutInstance(name: workoutName, guid: workoutTypeGuid, exerciseArray: [])
                
                
                storedWorkout.instanceGuid = instanceGuid
                storedWorkout.dateString = instanceDateString
                output.append(storedWorkout)
            }
            
            
        } catch {
            print("DEBUG: failed to retrieve workouts")
        }
        
        
        
        return output
        
    }
    
    public func fetchWorkoutInstanceName(parentGuid : String) -> String {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Workouts")
        request.predicate = NSPredicate(format: "guid = %@", parentGuid)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            let array = results as! [NSManagedObject]
            let result = array[0]
            let name = result.value(forKey: "name") as! String
            return name
        } catch {
                print("DEBUG: name not found")
        }
        
        return ""
    }
    
    public func fetchExeciseInstanceName(parentGuid : String) -> String {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercises")
        request.predicate = NSPredicate(format: "guid = %@", parentGuid)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            let array = results as! [NSManagedObject]
            let result = array[0]
            let name = result.value(forKey: "name") as! String
            return name
        } catch {
            print("DEBUG: name not found")
        }
        
        return ""
    }
    
    public func fetchWorkoutInstanceDate(guid : String) -> String {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WorkoutInstances")
        request.predicate = NSPredicate(format: "guid = %@", guid)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            let array = results as! [NSManagedObject]
            let result = array[0]
            let date = result.value(forKey: "dateFinished") as! Date
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date
            formatter.dateFormat = "dd-MMM-yyyy"
            // again convert your date to string
            let instanceDateString = formatter.string(from: date)
            return instanceDateString
        } catch {
            print("DEBUG: date not found")
        }
        
        return ""
    }
    
    public func fetchWorkoutInstance(_ guid : String) -> WorkoutInstance {
        
        
        var output : WorkoutInstance?
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "WorkoutInstances")
        request.predicate = NSPredicate(format: "guid = %@", guid)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            let resultArray = results as! [NSManagedObject]
            let result = resultArray[0]
            let parentWorkoutGuid = result.value(forKey: "workoutTypeGuid") as! String
            let name = fetchWorkoutInstanceName(parentGuid: parentWorkoutGuid)
            let date = fetchWorkoutInstanceDate(guid: guid)
            let soleExercise = Exercise()
            
            // fetch exercise instances
            do {
                
                let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseInstances")
                request2.predicate = NSPredicate(format: "parentWorkoutInstance = %@", guid)
                request2.returnsObjectsAsFaults = false
                
                let results2 = try context.fetch(request2)
                let resultArray2 = results2 as! [NSManagedObject]
                
                if resultArray2.count > 0 {
                    for e in resultArray2 {
                        
                        
                        let guid = e.value(forKey: "guid") as! String
                        let parentExerciseGuid = e.value(forKey: "exerciseTypeGuid") as! String
                        let name = self.fetchExeciseInstanceName(parentGuid: parentExerciseGuid)
                        let reps = e.value(forKey: "reps") as! Int
                        let weight = e.value(forKey: "weight") as! Double
                        
                        let exerciseInstance = ExerciseInstance(name: name, guid: parentExerciseGuid, reps: reps, weight: weight, instanceGuid: guid)
                        
                        soleExercise.completedExercises.append(exerciseInstance)
                        print("appended!!!!!")
                    }
                    print("finished appending!!")
                } else {
                    print("0 results")
                }
                
                
                
            } catch {
                print("DEBUG: Error fetching exerciseInstances")
            }
            
            
            output = WorkoutInstance(name: name, guid: parentWorkoutGuid)
            output?.exerciseArray = [soleExercise]
            output?.dateString = date
            
            
        } catch {
            print("DEBUG: Error Fetching workoutInstance")
        }
        
        return output!
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
