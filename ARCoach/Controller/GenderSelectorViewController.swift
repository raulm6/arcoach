//
//  GenderSelectorViewController.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class GenderSelectorViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var picker: UIPickerView!
    
    var tableObject : MyBodyTableObject?
    
    let genderArray : [String] = [
        "Female",
        "Male",
        "Other"
    ]
    
    @IBAction func confirmPressed(_ sender: UIButton) {
        
        let selectedValue = genderArray[picker.selectedRow(inComponent: 0)]
        
        // Pass Value to Table Source
        self.tableObject?.fieldDictionary["gender"] = selectedValue
        
        // Save to CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.coreDataController?.saveTableObjectToUser(tableObject: self.tableObject!, guid: (appDelegate.ARCoachUserSession?.guid)!)
        
        
        self.dismiss(animated: true)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.picker.delegate = self
        self.picker.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
