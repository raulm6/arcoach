//
//  File.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/25/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MyBodyTableObject {
    var fieldDictionary : [String : Any?] = [
        "height" : nil,
        "weight" : nil,
        "dateOfBirth" : nil,
        "gender" : nil
    ]
    
    var weight : Double?
    var date : Date?
    
    var fieldOrderArray : [String]
    
    init() {
        fieldOrderArray = [
            "height",
            "weight",
            "dateOfBirth",
            "gender"
        ]
        weight = nil
        date = nil
    }
    
    func populateWithUserData(guid: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let userObject = appDelegate.coreDataController?.getUserEntity(guid: guid)
        if let user = userObject {
            if let height = user.value(forKey: "height") as! Double? {
                self.fieldDictionary["height"] = String(Int(height))
            }
            
            if let weight = user.value(forKey: "weight") as! Double? {
                self.fieldDictionary["weight"] = String(weight)
            }
            
            if let dateOfBirth = user.value(forKey: "dateOfBirth") as! Date? {
                self.date = dateOfBirth
            }
            
            if let gender = user.value(forKey: "gender") as! String? {
                self.fieldDictionary["gender"] = gender
            }
            
        }
        
    }
    
    
    
}
