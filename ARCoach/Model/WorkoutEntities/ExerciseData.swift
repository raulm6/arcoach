//
//  ExerciseData.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/28/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation
import UIKit

class ExerciseData {
    
    public var exerciseDictionary : [String : Exercise]
    
    init() {
        
        let deadlift = Exercise(name: "Deadlift", guid: "baf539de-a03e-41dd-9c51-9ff71efdc8c3")
        
        let backsquat = Exercise(name: "Back Squat", guid: "c327fcb2-f304-4cb3-aabc-c34c8c00f6f4")
        
        let jeffersonsquat = Exercise(name: "Jefferson Squat", guid: "667abe5e-4417-45f5-9358-6384e041fb9e")
        
        let frontsquat = Exercise(name: "Front Squat", guid: "4469702c-0893-48f6-9fb2-1ea6dafc1933")
        
        let pullup = Exercise(name: "Pull up", guid: "77efc9fe-f578-4c37-b09e-bd1bb2846ce5")
        
        let bentoverrow = Exercise(name: "Bent Over Row", guid: "4d04cdd1-2a89-4a72-9303-7c9e77861556")
        
        let rackpull = Exercise(name: "Rack Pull", guid: "dafb9320-f978-42b1-8285-8c2fc67da6ac")
        
        let flatbench = Exercise(name: "Flat Bench", guid: "49e632a8-7946-47f2-83e4-c34ccb22fb3b")
        
        let declinebench = Exercise(name: "Decline Bench", guid: "5cbd3566-d4bf-42df-ae21-6d705b41d7a5")
        
        let sprint = Exercise(name: "Sprint", guid: "ae117807-4277-4779-91d5-1cb566bbcc9c")
        
        
        exerciseDictionary = [
            "Deadlift" : deadlift,
            "Back Squat" : backsquat,
            "Jefferson Squat" : jeffersonsquat,
            "Front Squat" : frontsquat,
            "Pull up" : pullup,
            
             bentoverrow.name : bentoverrow,
             rackpull.name : rackpull,
             flatbench.name : flatbench,
             declinebench.name : declinebench,
             sprint.name : sprint
            
            
        ]
    }
    
}
