//
//  WorkoutData.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/28/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation
import UIKit

class WorkoutData {
    
    
    var workoutDictionary : [String : Workout]
    
    init() {
        
        workoutDictionary = [String : Workout]()
        
        let exerciseDict = ExerciseData().exerciseDictionary
        
        
        let w1Exercises = [
            exerciseDict["Deadlift"]!,
            exerciseDict["Front Squat"]!,
            exerciseDict["Jefferson Squat"]!,
            exerciseDict["Back Squat"]!,
            exerciseDict["Pull up"]!
            
        
        ]
        
        let w1 = Workout(name : "Day 01", guid : "a8cd314c-936a-460b-bb42-4a7dccf6a6", exerciseArray : w1Exercises)
        
        workoutDictionary["Day 01"] = w1
        
        // Day 02
        
        let w2Exercises = [
            exerciseDict["Bent Over Row"]!,
            exerciseDict["Rack Pull"]!,
            exerciseDict["Flat Bench"]!,
            exerciseDict["Decline Bench"]!,
            exerciseDict["Sprint"]!
            
        ]
        
        let w2 = Workout(name : "Day 02", guid : "5e5d3c75-b410-4e5a-b4cb-eee2abbbab7d", exerciseArray : w2Exercises)
        
        workoutDictionary["Day 02"] = w2
        
    }            
}
