//
//  ExerciseInstance.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/29/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation

class ExerciseInstance : Exercise {
    
    var instanceGuid : String = ""
    var reps : Int = 0
    var weight : Double = 0.0
        
    init (name : String = "", guid : String = "", reps : Int = 0, weight : Double = 0.0, instanceGuid : String = UUID().uuidString) {
        super.init(name : name, guid : guid)
        self.reps = reps
        self.weight = weight
        self.instanceGuid = instanceGuid
    }
    
}
