//
//  Exercise.swift
//  ARCoach
//
//  Created by Raul Monraz on 10/30/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation
import CoreData

class Exercise : NSCopying {
    
    
    
    var name : String = ""
    var guid : String = ""
    var completedExercises : [ExerciseInstance] = []
    
    init(name : String = "", guid : String = "") {
        self.name = name
        self.guid = guid
        //super.init(entity entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?)

    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Exercise(name : self.name, guid : self.guid)
    }
    
    public func spawnExerciseInstance(reps : Int, weight : Double) -> ExerciseInstance {
        return ExerciseInstance(name : self.name, guid : self.guid, reps : reps, weight: weight, instanceGuid : UUID().uuidString)
    }
    
    public func spawnAndAddExerciseInstance(reps : Int, weight : Double) {
        completedExercises.append(ExerciseInstance(name : self.name, guid : self.guid, reps : reps, weight: weight, instanceGuid : UUID().uuidString))
    }
    
    
}
