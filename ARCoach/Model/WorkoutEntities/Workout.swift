//
//  Workout.swift
//  ARCoach
//
//  Created by Raul Monraz on 11/12/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Workout {
    
    var name : String = ""
    var guid : String = ""
    var exerciseArray : [Exercise] = []
    
    init() {
        
    }
    
    init(name : String = "", guid : String = "", exerciseArray : [Exercise] = []) {
        self.name = name
        self.guid = guid
        self.exerciseArray = exerciseArray
    }
    
    public func addExercise(exercise : Exercise) {
        exerciseArray.append(exercise)
    }
    
    public func spawnWorkoutInstance() -> WorkoutInstance {
        let instance =  WorkoutInstance(name : self.name, guid : self.guid)
        instance.exerciseArray = self.exerciseArray.map { $0.copy() as! Exercise}
        return instance
    }
    
    
}
