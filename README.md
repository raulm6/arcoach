# README #

This is the source code for ARCoach, an Fitness iOS application that uses Augmented Reality to teach the user proper 
weight lifting and exercising form while also allowing them to save their progress.

### How do I get set up? ###

To test, clone the repository and open in xcode, connect a physical device (Required for ARkit) and hit run.

